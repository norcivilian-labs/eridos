import { render } from "preact";
import { Root } from "./layout/root.jsx";

render(<Root />, document.getElementById("root"));
