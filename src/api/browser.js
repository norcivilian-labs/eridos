export class BrowserAPI {
  constructor() {}

  async login(email, password) {
    if ((email, password === undefined)) {
      console.error("login, password undefined");
    }

    try {
      const response = await fetch(
        "https://functions.yandexcloud.net/d4esr6ie5khkuno72hib",
        {
          method: "post",
          headers: { Accept: "application/json", "Content-Type": "text/plain" },
          body: JSON.stringify({
            op: "login",
            email,
            password,
          }),
        },
      );

      if (response.status === 200) {
        // Добавить обработку токена или переход к другому экрану после успешного входа
        const { token } = await response.json();

        // TODO: validate token

        return token;
      } else {
        console.error("Login failed");
        // Добавить обработку ошибок, например, отображение сообщения об ошибке
      }
    } catch (error) {
      console.error("Error during login:", error);
    }
  }
  async signup({
    email,
    password,
    city,
    country,
    firstname,
    surname,
    middlename,
    organization,
    profession,
    promocode,
  }) {
    try {
      const response = await fetch(
        "https://functions.yandexcloud.net/d4esr6ie5khkuno72hib",
        {
          method: "post",
          headers: { Accept: "application/json", "Content-Type": "text/plain" },
          body: JSON.stringify({
            op: "register",
            email,
            password,
            first_name: firstname,
            last_name: surname,
            middle_name: middlename,
            country: country,
            city: city,
            organization: organization,
            profession: profession,
            promocode: promocode,
            language: "en",
            domain: "com",
            name: "",
            real_name: "",
          }),
        },
      );

      if (response.status === 200) {
        // route('/login', true);
        // Дополнительные действия после успешной регистрации, например, переход на другой экран
      } else {
        console.error("Login failed");
        // Добавить обработку ошибок, например, отображение сообщения об ошибке
      }
    } catch (error) {
      console.error("Error during login:", error);
    }
  }

  async getUserData(token) {
    try {
      const response = await fetch(
        "https://functions.yandexcloud.net/d4esr6ie5khkuno72hib",
        {
          method: "post",
          headers: { Accept: "application/json", "Content-Type": "text/plain" },
          body: JSON.stringify({
            op: "get_info",
            token: token,
          }),
        },
      );

      const data = await response.json();

      return data;
    } catch (error) {
      console.error("Error during getting user data:", error);
    }
  }

  async sendFundus(token, webpDataUrl) {
    try {
      const response = await fetch(
        "https://functions.yandexcloud.net/d4e5t0njkd4f1mb9kh5l",
        {
          method: "post",
          headers: { Accept: "*/*", "Content-Type": "text/plain" },
          body: JSON.stringify({
            token: token,
            image: webpDataUrl,
          }),
        },
      );

      const data = await response.json();

      return data;
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  }
}
