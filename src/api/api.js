import { BrowserAPI } from "./browser.js";
import { invoke } from "@tauri-apps/api/core";

export class API {
  #browser;

  constructor() {
    this.#browser = new BrowserAPI();
  }

  async login(email, password) {
    if (import.meta.env.TAURI_ENV_ARCH != undefined) {
      return invoke("login", { email, password });
    } else {
      return this.#browser.login(email, password);
    }
  }

  async signup(signupData) {
    if (import.meta.env.TAURI_ENV_ARCH != undefined) {
      const response = await invoke("signup", { signupData });
      console.log(response);
    } else {
      return this.#browser.signup(signupData);
    }
  }

  async getUserData(token) {
    if (import.meta.env.TAURI_ENV_ARCH != undefined) {
      return invoke("get_info", { token });
    } else {
      return this.#browser.getUserData(token);
    }
  }

  async sendFundus(token, webpDataUrl) {
    if (import.meta.env.TAURI_ENV_ARCH != undefined) {
      return invoke("get_info", { token, webpDataUrl });
    } else {
      return this.#browser.sendFundus(token, webpDataUrl);
    }
  }
}
