import { v4 as uuidv4 } from "uuid";

export const createReportSlice = (set, get) => ({
  id: undefined,

  // bootstraps the state and reads the URL on application launch
  addReport: (report) => {
    const { reports } = get();

    const id = uuidv4();

    set({
      reports: { ...reports, [id]: report },
    });

    return id;
  },

  setID: async (id) => {
    set({ id });
  },
});
