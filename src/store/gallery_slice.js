export const createGallerySlice = (set, get) => ({
  reports: {
    1: { image: "", date: "2024-01-01", description: "a report" },
    2: { image: "", date: "2024-01-01", description: "a report" },
    3: { image: "", date: "2024-01-01", description: "a report" },
  },

  // bootstraps the state and reads the URL on application launch
  removeReport: async (id) => {
    const { reports } = get();

    set({
      reports: reports.filter((report) => id !== report.id),
    });
  },
});
