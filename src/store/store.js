import { create } from "zustand";
import { createGallerySlice } from "./gallery_slice.js";
import { createReportSlice } from "./report_slice.js";

export const useStore = create((...a) => ({
  ...createGallerySlice(...a),
  ...createReportSlice(...a),
}));
