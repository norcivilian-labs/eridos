import { h } from "preact";
import { LocationProvider, ErrorBoundary, Router } from "preact-iso";
import styles from "./root.module.css";
import { Navbar } from "@/components/index.js";
import { Detection } from "./detection/index.js";
import { Login } from "./login/index.js";
import { Profile } from "./profile/index.js";
import { Signup } from "./signup/index.js";
import { Gallery } from "./gallery/index.js";
import { useState, useEffect } from "preact/hooks";

export function Root() {
  const [token, setToken] = useState(sessionStorage.getItem("token"));

  const hasToken = token !== null;

  //  проверяем наличие токена в кэше
  // const checkToken = () => {
  //   const token = sessionStorage.getItem("token");
  //   const hasNotToken = token === null;
  //   if (hasNotToken) {
  //     route("/login", true);
  //   } else {
  //     setToken(token);
  //   }
  // };

  // Запрос данных при открытии экрана профиля
  // useEffect(() => {
  //   checkToken();
  // }, []);

  // После успешного входа сохраняем токен в кэше
  const handleLogin = async (token) => {
    try {
      sessionStorage.setItem("token", token);

      setToken(token);

      console.log("Токен успешно сохранен в кэше");
    } catch (error) {
      console.error("Ошибка входа:", error);
    }
  };

  return (
    <div>
      <LocationProvider>
        <ErrorBoundary>
          <Router>
            <Login path="/login" handleLogin={handleLogin} />

            <Gallery path="/" token={token} />

            <Detection path="/detection/:id" token={token} />

            <Detection path="/new" token={token} />

            <Profile path="/profile" token={token} />

            <Signup path="/signup" />
          </Router>
        </ErrorBoundary>
      </LocationProvider>

      <Navbar hasToken={hasToken} />
    </div>
  );
}
