import { useState, useEffect } from "preact/hooks";
import { route } from "preact-router";
import styles from "./profile.module.css";
import { API } from "../../api";

export function Profile({ token }) {
  // Добавляем переменные для каждого значения
  const [name, setName] = useState("");
  const [country, setCountry] = useState("");
  const [city, setCity] = useState("");
  const [email, setEmail] = useState("");
  const [profession, setProfession] = useState("");
  const [organization, setOrganization] = useState("");

  const menuUp = async () => {
    route("/", true);
  };

  // Функция для запроса данных
  const fetchData = async () => {
    try {
      const api = new API();
      const data = await api.getUserData(token);
      // Деструктурируем данные и устанавливаем переменные
      setName(data.name);
      setCountry(data.country);
      setCity(data.city);
      setEmail(data.email);
      setProfession(data.profession);
      setOrganization(data.organization);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // Запрос данных при открытии экрана профиля
  useEffect(() => {
    fetchData();
  }, [token]); // Пустой массив означает, что эффект выполнится только при монтировании компонента

  return (
    <div className={styles.container}>
      {/* section button */}
      <button type="button" onClick={menuUp} className="exit">
        Выйти
      </button>
      <div className={styles.profile}>
        {/* profile name */}
        <h1 style={{ fontSize: "6vh" }}>{name}</h1>

        {/* section About me */}
        <h2 style={{ fontSize: "4vh" }}>Обо мне</h2>
        <p className="country" style={{ fontSize: "2vh" }}>
          Страна: {country}
        </p>
        <p className="city" style={{ fontSize: "2vh" }}>
          Город: {city}
        </p>
        <p className="email" style={{ fontSize: "2vh" }}>
          Email: {email}
        </p>

        {/* section work and education */}
        <h3 style={{ fontSize: "4vh" }}>Работа и образование</h3>
        <p className="profession" style={{ fontSize: "2vh" }}>
          Профессия: {profession}
        </p>
        <p className="organization" style={{ fontSize: "2vh" }}>
          Организация: {organization}
        </p>
      </div>
    </div>
  );
}
