import Konva from "konva";
import { useState, useRef, useCallback } from "preact/hooks";
import { useRoute, useLocation } from "preact-iso";
import { useStore } from "@/store/index.js";
import styles from "./detection.module.css";
import { API } from "../../api";

async function urlToWebpDataUrl(url) {
  const imageElement = new Image();
  imageElement.src = url;

  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");

  return new Promise((res) => {
    imageElement.onload = () => {
      // Setting the canvas dimensions to match the image dimensions
      canvas.width = imageElement.width;
      canvas.height = imageElement.height;

      // Draw an image on the canvas
      ctx.drawImage(imageElement, 0, 0);

      // Getting the data URL of the image in webp format
      const webpDataUrl = canvas.toDataURL("image/webp");

      res(webpDataUrl);
    };
  });
}

export function Detection({ token }) {
  const { route } = useLocation();

  const { path } = useRoute();

  const isNew = path === "/new";

  const [reports, id, addReport, setID] = useStore((state) => [
    state.reports,
    state.id,
    state.addReport,
    state.setID,
  ]);

  const menuUp = async () => {
    route("/", true);

    setID(undefined);
  };

  const report = reports[id] ?? undefined;

  const [contours, setContours] = useState({});

  const api = new API();

  const container = useRef(null);

  const setRef = useCallback(
    async (node) => {
      if (container.current) {
        // Make sure to cleanup any events/references added to the last instance
      }

      if (node) {
        if (report && report.image) {
          const imageElement = new Image();

          imageElement.src = report.image;

          const { width, height } = await new Promise((res) => {
            imageElement.onload = () => {
              res({ width: imageElement.width, height: imageElement.height });
            };
          });

          // Check if a node is actually passed. Otherwise node would be null.
          // You can now do what you need to, addEventListeners, measure, etc.
          const stage = new Konva.Stage({
            container: node, // id of container <div>
            width,
            height,
          });

          var layer = new Konva.Layer();

          stage.add(layer);

          const image = new Konva.Image({
            x: 0,
            y: 0,
            image: imageElement,
            width,
            height,
          });

          layer.add(image);

          for (const [key, data] of Object.entries(contours)) {
            for (const contour of data) {
              const line = new Konva.Line({
                points: contour,
                fill: "#00D2FF",
                stroke: "black",
                strokeWidth: 2,
                closed: true,
              });

              layer.add(line);
            }
          }
        }
      }

      // Save a reference to the node
      container.current = node;
    },
    [id, contours],
  );

  async function onUpload(f) {
    if (f) {
      const url = URL.createObjectURL(f);

      const webpDataUrl = await urlToWebpDataUrl(url);

      const data = await api.sendFundus(token, webpDataUrl);

      const reportNew = {
        image: url,
        description: "added report",
        date: "2024-03-03",
        data,
      };

      const id = addReport(reportNew);

      setID(id);

      route(`/detection/${id}`, true);
    }
  }

  function toggleContour(key, data) {
    if (contours[key] !== undefined) {
      const { [key]: removed, ...contoursNew } = contours;

      setContours(contoursNew);
    } else {
      setContours({ ...contours, [key]: data });
    }
  }

  return (
    // basic container
    <div className={styles.container}>
      {/* main content */}
      <div className={styles.detection}>
        <button type="button" onClick={menuUp} className={styles.exit}>
          Выйти
        </button>

        {isNew && (
          <input type="file" onChange={(e) => onUpload(e.target.files[0])} />
        )}

        {report && (
          <div>
            <p className={styles.сontent}> your photo:</p>

            <p>{report.date}</p>

            <p>{report.description}</p>

            <div ref={setRef} />

            {/* buttons with the names of deviations */}
            {report.data && (
              <div className={styles.deviations}>
                {Object.keys(report.data).map((key, index) => (
                  <button
                    key={index}
                    type="button"
                    className={styles.deviation}
                    onClick={() => toggleContour(key, report.data[key])}
                  >
                    {key}
                  </button>
                ))}
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
}
