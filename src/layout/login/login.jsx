import { useState } from "preact/hooks";
import { useLocation } from "preact-iso";
import styles from "./login.module.css";
import { API } from "../../api";

export function Login({ handleLogin }) {
  const { route } = useLocation();

  const [email, setEmail] = useState(undefined);

  const [password, setPassword] = useState(undefined);

  const api = new API();

  const signUp = async () => {
    route("/signup", true);
  };

  const login = async () => {
    const token = await api.login(email, password);

    handleLogin(token);

    route("/", true);
  };

  return (
    <div className={styles.login}>
      <h1 className="heading">Retina.Al</h1>

      <input
        type="email"
        name="email"
        id="email"
        placeholder="Email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />

      <input
        type="password"
        name="password"
        id="password"
        placeholder="Пароль"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />

      <a href="#">Забыли пароль</a>

      <button type="button" onClick={login}>
        Войти
      </button>

      <button type="button" onClick={signUp}>
        Зарегистрироваться
      </button>
    </div>
  );
}
