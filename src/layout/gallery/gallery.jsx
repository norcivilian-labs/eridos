import { useLocation } from "preact-iso";
import styles from "./gallery.module.css";
import { useStore } from "@/store/index.js";
import { PlusIcon } from "./components/index.js";

export function Gallery({ token }) {
  const { route } = useLocation();

  const hasToken = token !== null;

  const [reports, setID] = useStore((state) => [state.reports, state.setID]);

  return (
    <div className={styles.menu}>
      <a className={styles.link} href="/new">
        <img src={PlusIcon} />
      </a>

      <div style={{ display: "flex" }}>
        {Object.entries(reports).map(([id, report], idx) => (
          <div
            key={idx}
            onClick={() => {
              setID(id);

              route(`/detection/${id}`);
            }}
          >
            <img src={report.image} />

            <p>{report.date}</p>

            <p>{report.description}</p>
          </div>
        ))}
      </div>
    </div>
  );
}
