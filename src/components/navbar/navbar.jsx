import React from "preact/compat";
import { useRoute } from "preact-iso";
import styles from "./navbar.module.css";
import {
  GalleryIcon,
  GalleryBlueIcon,
  LoginIcon,
  LoginBlueIcon,
  ProfileIcon,
  ProfileBlueIcon,
} from "./components/index.js";

export function Navbar({ hasToken }) {
  const { path } = useRoute();

  return (
    <div className={styles.wrapper}>
      <a className={styles.button__wrapper} href="/">
        <img src={path === "/" ? GalleryBlueIcon : GalleryIcon} />

        <div className={styles.button__text}>Gallery</div>
      </a>

      {hasToken ? (
        <a className={styles.button__wrapper} href="profile">
          <img src={path === "/profile" ? ProfileBlueIcon : ProfileIcon} />

          <div className={styles.button__text}>Profile</div>
        </a>
      ) : (
        <a className={styles.button__wrapper} href="login">
          <img src={path === "/login" ? LoginBlueIcon : LoginIcon} />

          <div className={styles.button__text}>Log in</div>
        </a>
      )}
    </div>
  );
}
