{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = inputs@{ nixpkgs, ... }:
    let
      eachSystem = systems: f:
        let
          op = attrs: system:
            let
              ret = f system;
              op = attrs: key:
                let
                  appendSystem = key: system: ret: { ${system} = ret.${key}; };
                in attrs // {
                  ${key} = (attrs.${key} or { })
                    // (appendSystem key system ret);
                };
            in builtins.foldl' op attrs (builtins.attrNames ret);
        in builtins.foldl' op { } systems;
      defaultSystems = [ "x86_64-linux" "aarch64-darwin" "aarch64-linux" ];
    in eachSystem defaultSystems (system:
      let
        overlays = [ inputs.fenix.overlays.default ];
        pkgs = import nixpkgs { inherit system overlays; };
        src = pkgs.nix-gitignore.gitignoreSource [ ".git" ] ./.;
        package = (pkgs.lib.importJSON (src + "/package.json"));
        nodeVersion =
          builtins.elemAt (pkgs.lib.versions.splitVersion pkgs.nodejs.version)
          0;
        crane = rec {
          lib = inputs.crane.lib.${system};
          stable = lib.overrideToolchain
            inputs.fenix.packages.${system}.minimal.toolchain;
        };
        library = pkgs.mkYarnPackage rec {
          name = package.name;
          version = package.version;
          src = pkgs.nix-gitignore.gitignoreSource [ ".git" ] ./.;
          buildPhase = ''
            true
          '';
          # fixup of libexec hangs for undiscovered reason
          dontStrip = true;
        };
        webappDrv = buildMode:
          pkgs.mkYarnPackage rec {
            name = package.name;
            version = package.version;
            src = pkgs.nix-gitignore.gitignoreSource [ ".git" ] ./.;
            preConfigure = ''
              substituteInPlace package.json --replace "vite build" "yarn exec vite build"
            '';
            buildPhase = ''
              yarn run build
            '';
            installPhase = "cp -r ./deps/${name}/dist $out";
            distPhase = "true";
          };
        webapp = webappDrv "webapp";
      in rec {
        packages = { inherit webapp; };
        defaultPackage = packages.webapp;
        defaultApp = packages.webapp;
        devShell = pkgs.mkShell {
          nativeBuildInputs = [ pkgs.wrapGAppsHook ];
          buildInputs = with pkgs;
            [
              #(fenix.complete.withComponents [
              #  "cargo"
              #  "clippy"
              #  "rust-src"
              #  "rustc"
              #  "rustfmt"
              #])
              (fenix.combine [
                fenix.minimal.cargo
                fenix.minimal.rustc
                fenix.targets.x86_64-apple-darwin.stable.rust-std
                fenix.targets.aarch64-apple-darwin.stable.rust-std
              ])
              rust-analyzer-nightly
              pkg-config
              yarn
              libiconv
            ] ++ (if system == "aarch64-darwin" then
              with darwin.apple_sdk.frameworks; [
                SystemConfiguration
                Carbon
                WebKit
                cocoapods
              ]
            else
              [ ]) ++ (if system == "x86_64-linux" || system == "aarch64-linux"
              || system == "x86_64-unknown-linux-gnu" then [
                curl
                wget
                dbus
                openssl_3
                libsoup_3
                webkitgtk
                librsvg
                gtk3
                gtk4
                glib
                webkitgtk_4_1
                gdk-pixbuf
                pango
                libadwaita
                sqlite
              ] else
                [ ]);
        };
        libraries = with pkgs; [
          webkitgtk
          gtk3
          cairo
          gdk-pixbuf
          glib
          dbus
          openssl_3
          librsvg
        ];
        shellHook = ''
          export XDG_DATA_DIRS=$GSETTINGS_SCHEMA_PATH
        '';
      });
}
