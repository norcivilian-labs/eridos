import js from "@eslint/js"
import react from "eslint-plugin-react"
import globals from "globals"
import eslintConfigPrettier from "eslint-config-prettier";

export default [
  js.configs.recommended,
  {
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node
      },
    },
    plugins: {
      react,
    },
    settings: {
      react: {
        pragma: "h"
      }
    }
  },
  eslintConfigPrettier,
]
