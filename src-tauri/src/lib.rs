// use reqwest;
use serde::{Deserialize, Serialize};
use serde_json;
use std::collections::HashMap;

// struct for login
#[derive(Deserialize, Debug)]
struct Login {
    token: String,
    confirmed: bool,
}

// structs for signup
#[derive(Serialize, Deserialize, Debug)]
struct Signup {
    op: String,
    email: String,
    password: String,
    city: String,
    country: String,

    #[serde(rename(serialize = "first_name"))]
    firstname: String,

    #[serde(rename(serialize = "last_name"))]
    surname: String,

    #[serde(rename(serialize = "middle_name"))]
    middlename: String,

    organization: String,
    profession: String,
    promocode: String,
    language: String,
    domain: String,
    name: String,
    real_name: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct SignupResponse {
    ok: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct UserInformation {
    accepted_license: bool,
    accepted_oct_license: bool,
    can_use_admin: bool,
    can_use_fundus: bool,
    can_use_oct: bool,
    can_use_trainer: bool,
    city: String,
    confirmed: bool,
    country: String,
    domain: String,
    email: String,
    first_name: String,
    fundus_processed_per_day: Vec<i32>,
    fundus_processed_per_month: Vec<i32>,
    fundus_processed_per_year: Vec<i32>,
    fundus_visits_per_day: Vec<i32>,
    fundus_visits_per_month: Vec<i32>,
    fundus_visits_per_year: Vec<i32>,
    id: String,
    last_activity: String,
    last_name: String,
    middle_name: String,
    name: String,
    oct_processed_per_day: Vec<i32>,
    oct_processed_per_month: Vec<i32>,
    oct_processed_per_year: Vec<i32>,
    oct_visits_per_day: Vec<i32>,
    oct_visits_per_month: Vec<i32>,
    oct_visits_per_year: Vec<i32>,
    organization: String,
    profession: String,
    promocodes: Vec<String>,
    real_name: String,
    reg_date: String,
    reported_per_day: Vec<i32>,
    reported_per_month: Vec<i32>,
    reported_per_year: Vec<i32>,
    reviews_per_day: Vec<i32>,
    reviews_per_month: Vec<i32>,
    reviews_per_year: Vec<i32>,
    trainer_visits_per_day: Vec<i32>,
    trainer_visits_per_month: Vec<i32>,
    trainer_visits_per_year: Vec<i32>,
    uploaded_per_day: Vec<i32>,
    uploaded_per_month: Vec<i32>,
    uploaded_per_year: Vec<i32>,
}

#[derive(Serialize, Deserialize, Debug)]
struct FundusData {
    exudates_in_macula: bool,
    exudates_in_fovea: bool,
    height: i32,
    width: i32,
    image: String,
    macula: Vec<f32>,
    hard_exudates: Vec<Vec<i32>>,
    intraretinal_hemorrhages: Vec<Vec<i32>>,
    soft_exudates: Vec<Vec<i32>>,
    fibrose: Vec<Vec<i32>>,
    laser: Vec<Vec<i32>>,
    microaneurysms: Vec<Vec<i32>>,
    neovascularization: Vec<Vec<i32>>,
    preretinal: Vec<Vec<i32>>,
    va: Vec<Vec<i32>>,
}


// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
// TO DO:
// error handling when confirmed = false (when user is not logged in)
fn login(email: &str, password: &str) -> Result<String, String> {
    // println!("login, {}, {}", email, password);

    // let client = reqwest::blocking::Client::new();

    // let mut map = HashMap::new();
    // map.insert("op", "login");
    // map.insert("email", email);
    // map.insert("password", password);

    // // let json_string: String = serde_json::to_string(&map).unwrap();

    // let json_result: Result<String, serde_json::Error> = serde_json::to_string(&map);
    // let json_string: String = match json_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };


    // // println!("{}", json_string);

    // let resp_result = client
    //     .post("https://functions.yandexcloud.net/d4esr6ie5khkuno72hib")
    //     // .json(&map)
    //     .body(json_string)
    //     .send();

    // let resp = match resp_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };

    // // println!("{resp:#?}");

    // // let resp_string: String = resp.text().unwrap();
    // // println!("{resp_string:#?}");

    // let resp_struct_result: Result<Login, reqwest::Error> = resp.json::<Login>();

    // let resp_struct = match resp_struct_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };



    // // println!("{resp_struct:#?}");

    // // format!("Hello, {}! You've been greeted from Rust!", email)
    // Ok(resp_struct.token)
    Ok("".to_string())
}

#[tauri::command]
fn signup(signup_data: Signup) -> Result<String, String> {
    // println!("SIGNUP");
    // let status = "ok from sign up".to_string();
    // println!("{status}");

    // let client = reqwest::blocking::Client::new();

    // let json_result: Result<String, serde_json::Error> = serde_json::to_string(&signup_data);
    // let json_string = match json_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };


    // // println!("{}", json_string);
    // let resp_result = client
    //     .post("https://functions.yandexcloud.net/d4esr6ie5khkuno72hib")
    //     .body(json_string)
    //     .send();

    // let resp = match resp_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };



    // let signup_result = resp.json::<SignupResponse>();

    // let signup_response = match signup_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };


    // // println!("{:#?}", signup_resp);
    // let status = "ok from sign up".to_string();
    // Ok(status)
    Ok("".to_string())
}

// profile information on login
#[tauri::command]
fn get_info(token: &str) -> Result<UserInformation, String> {
    // let client = reqwest::blocking::Client::new();

    // let mut user_data = HashMap::new();
    // user_data.insert("op", "get_info");
    // user_data.insert("token", token);

    // let data_result = serde_json::to_string(&user_data);
    // let string_data = match data_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };
    // // println!("string_data = {}", string_data);

    // let resp_result = client
    //     .post("https://functions.yandexcloud.net/d4esr6ie5khkuno72hib")
    //     .body(string_data)
    //     .send();

    // let resp = match resp_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };


    // let response_struct_result = resp.json::<UserInformation>();
    // let response_struct = match response_struct_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };
    // Ok(response_struct)
    Err("".to_string())
}

#[tauri::command]
fn send_fundus(token: &str, image: &str) -> Result<FundusData, String> {
    // let client = reqwest::blocking::Client::new();

    // let mut fundus_data = HashMap::new();
    // fundus_data.insert("token", token);
    // fundus_data.insert("image", image);

    // println!("{:#?}", fundus_data);

    // let fundus_str_result = serde_json::to_string(&fundus_data);
    // let fundus_str = match fundus_str_result {
    //     Ok(res) => res,
    //     Err(err) => return Err(err.to_string()),
    // };
    // println!("{:#?}", fundus_str);

    // let fundus_response_result = client
    //     .post("https://functions.yandexcloud.net/d4e5t0njkd4f1mb9kh5l")
    //     .body(fundus_str)
    //     .send();

    // let fundus_response = match fundus_response_result {
    //     Ok(result) => result,
    //     Err(err) => return Err(err.to_string()),
    // };
    // println!("{:#?}", fundus_response);

    // let fundus_response_struct_result = fundus_response.json::<FundusData>();
    // let fundus_response_struct = match fundus_response_struct_result {
    //     Ok(result) => result,
    //     Err(error) => return Err(error.to_string()),
    // };

    // println!("{:#?}", fundus_response_struct);
    // Ok(fundus_response_struct)
    Err("".to_string())
}

#[cfg_attr(mobile, tauri::mobile_entry_point)]
pub fn run() {
    tauri::Builder::default()
        .plugin(tauri_plugin_shell::init())
        .invoke_handler(tauri::generate_handler![login, signup, get_info, send_fundus])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
